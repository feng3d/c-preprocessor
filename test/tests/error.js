/*

Test the #error directive

© 2016 - Guillaume Gonnet
License GPLv2

Sources at https://github.com/ParksProjets/C-Preprocessor

*/

// Create the test
const utils = require('../utils.js');
const test = utils.test('#error');

// Expected the rsult
const text = 'Math is the life !';

// Store error and success functions
const error = test.error;
const success = test.success;

// Override error function for checking if the right error is called
test.error = function (err)
{
	const msg = `compiler error -> (line 3 in "main") ${text}`;

	if (err === msg)
	{ success.call(test); }
	else
	{ error.call(test, 'we didn\'t get the right message'); }
};

// Override success function
test.success = function (_err)
{
	error.call(test, 'no error raised');
};

// Run the test
test.run(`

#error ${text}

`);
