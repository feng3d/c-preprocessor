/*

Test the #define directive for defining a variable

© 2016 - Guillaume Gonnet
License GPLv2

Sources at https://github.com/ParksProjets/C-Preprocessor

*/

// Create the test
const utils = require('../utils.js');
	const test = utils.test('#define variable');

// Random numbers
const a = utils.randint(1, 100);
	const b = utils.randint(1, 100);
	const c = utils.randint(200, 300);

// Expected results
test.result('r1', a + b);
test.result('r2', c);
test.result('r3', a * a + a);
test.result('today', new Date().toLocaleDateString());

// Predefined constants
test.setting('constants', {
	VARIABLE_3: c
});

// Run the test
test.run(`

#define VARIABLE ${a} // This is a comment
#define VARIABLE_2 ${b}

var r1 = VARIABLE + VARIABLE_2,
	r2 = VARIABLE_3,
	r3 = VARIABLE * VARIABLE + VARIABLE;

var today = "__DATE__";

`);
