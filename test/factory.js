/*

Test factory

© 2016 - Guillaume Gonnet
License GPLv2

Sources at https://github.com/ParksProjets/C-Preprocessor

*/

// Libraries
const compiler = require('../lib/index.js');
const vm = require('vm');

// Test class
class Test
{
	constructor(name)
	{
		this.name = name;
		this.results = {};
		this.settings = {};
	}
	// Log a success
	success()
	{
		console.log(`\x1b[1;32m${this.name} test passed\x1b[0m\n`);
	}
	// Log an error
	error(error)
	{
		console.log(`\x1b[1;31m${this.name} test error`);

		if (error) { console.log(`\x1b[1;31m  Error: ${error}`); }

		console.log('\x1b[0m');
	}
	// Add a result
	result(name, value)
	{
		this.results[name] = value;
	}
	// Set a setting
	setting(name, value)
	{
		this.settings[name] = value;
	}
	// Run the test
	run(code)
	{
		const _this = this;

		// Run the code and check the results
		function run(err, code)
		{
			// If there is a compiler error
			if (err) { return _this.error(`compiler error -> ${err}`); }

			// Run the code
			const sandbox = {};
			try
			{
				vm.runInNewContext(code, sandbox);
			}
			catch (e)
			{
				return _this.error(`code execution failed -> ${e.message}`);
			}

			// Check the results
			for (const n in _this.results)
			{
				const e = _this.results[n];
				const r = sandbox[n];

				if (e !== r) { return _this.error(`the expected value of variable ${n} was '${e}' but we got '${r}'`); }
			}

			// All is OK: log the success
			_this.success();
		}

		// Compile the code
		try
		{
			compiler.compile(code, this.settings, run);
		}
		catch (e)
		{
			this.error(`compiler execution failed -> ${e.message}`);
			console.log(e);
		}
	}
}

exports.Test = Test;
